/* *********************Tabulka symbolu********************************
 * Subor:      ial.c
 * Datum:      19.11.2014
 * Predmet:    Formalne jazyky a prekladace (IFJ)
 * Projekt:    Implementacia interpretu jazyka IFJ14
 * Autori:     Martin Kacmarcik xkacma00
 *             Roman Nedelka    xnedel07
 *             Natalia Jurdova  xjurdo00
 *             Marek Skalnik    xskaln03
 * ********************************************************************/

/* c016.c: **********************************************************}
{* Téma:  Tabulka s Rozptýlenými Polo¾kami
**                      První implementace: Petr Pøikryl, prosinec 1994
**                      Do jazyka C prepsal a upravil: Vaclav Topinka, 2005
**                      Úpravy: Karel Masaøík, øíjen 2014
**                      Úpravy: Radek Hranický, øíjen 2014
**
** Vytvoøete abstraktní datový typ
** TRP (Tabulka s Rozptýlenými Polo¾kami = Hash table)
** s explicitnì øetìzenými synonymy. Tabulka je implementována polem
** lineárních seznamù synonym.
**
** Implementujte následující procedury a funkce.
**
**  HTInit ....... inicializuje tabulku pøed prvním pou¾itím
**  HTInsert ..... vlo¾ení prvku
**  HTSearch ..... zji¹tìní pøítomnosti prvku v tabulce
**  HTDelete ..... zru¹ení prvku
**  HTRead ....... pøeètení hodnoty prvku
**  HTClearAll ... zru¹ení obsahu celé tabulky (inicializace tabulky
**                 poté, co ji¾ byla pou¾ita)
**
** Definici typù naleznete v souboru c016.h.
**
** Tabulka je reprezentována datovou strukturou typu tHTable,
** která se skládá z ukazatelù na polo¾ky, je¾ obsahují slo¾ky
** klíèe 'key', obsahu 'data' (pro jednoduchost typu float), a
** ukazatele na dal¹í synonymum 'ptrnext'. Pøi implementaci funkcí
** uva¾ujte maximální rozmìr pole HTSIZE.
**
** U v¹ech procedur vyu¾ívejte rozptylovou funkci hashCode.  Pov¹imnìte si
** zpùsobu pøedávání parametrù a zamyslete se nad tím, zda je mo¾né parametry
** pøedávat jiným zpùsobem (hodnotou/odkazem) a v pøípadì, ¾e jsou obì
** mo¾nosti funkènì pøípustné, jaké jsou výhody èi nevýhody toho èi onoho
** zpùsobu.
**
** V pøíkladech jsou pou¾ity polo¾ky, kde klíèem je øetìzec, ke kterému
** je pøidán obsah - reálné èíslo.
*/

#include "ial.h"

int HTSIZE = MAX_HTSIZE;
int solved;

/*          -------
** Rozptylovací funkce - jejím úkolem je zpracovat zadaný klíè a pøidìlit
** mu index v rozmezí 0..HTSize-1.  V ideálním pøípadì by mìlo dojít
** k rovnomìrnému rozptýlení tìchto klíèù po celé tabulce.  V rámci
** pokusù se mù¾ete zamyslet nad kvalitou této funkce.  (Funkce nebyla
** volena s ohledem na maximální kvalitu výsledku). }
*/

int hashCode ( tKey key ) {
	int i;
	int retval = 1;
	int keylen = strlen(key); //zde je delka klice
	for ( i=0; i<keylen; i++ )
		retval += tolower(key[i]);
	return ( retval % HTSIZE );
}

/*
** Inicializace tabulky s explicitnì zøetìzenými synonymy.  Tato procedura
** se volá pouze pøed prvním pou¾itím tabulky.
*/

void htInit ( tHTable* ptrht ) {
    int i;
    if((*ptrht) == NULL)
    {
        return;
    }
    else
    {
        for (i = 0; i < HTSIZE; i++)
        {
            (*ptrht)[i] = NULL;
        }
    }
}

/* TRP s explicitnì zøetìzenými synonymy.
** Vyhledání prvku v TRP ptrht podle zadaného klíèe key.  Pokud je
** daný prvek nalezen, vrací se ukazatel na daný prvek. Pokud prvek nalezen není,
** vrací se hodnota NULL.
**
*/

tHTItem* htSearch ( tHTable* ptrht, tKey key ) {

    if ((*ptrht) == NULL)
        return NULL;
    else
    {
        int index = hashCode(key);
        tHTItem *tmp = (*ptrht)[index];

        while(tmp != NULL)
        {
            if(strcmp(tmp->key, key) == 0)
            {
                return tmp;
            }

            tmp = tmp->ptrnext;
        }
        return NULL;
    }
}

/*
** TRP s explicitnì zøetìzenými synonymy.
** Tato procedura vkládá do tabulky ptrht polo¾ku s klíèem key a s daty
** data.  Proto¾e jde o vyhledávací tabulku, nemù¾e být prvek se stejným
** klíèem ulo¾en v tabulce více ne¾ jedenkrát.  Pokud se vkládá prvek,
** jeho¾ klíè se ji¾ v tabulce nachází, aktualizujte jeho datovou èást.
**
** Vyu¾ijte døíve vytvoøenou funkci htSearch.  Pøi vkládání nového
** prvku do seznamu synonym pou¾ijte co nejefektivnìj¹í zpùsob,
** tedy proveïte.vlo¾ení prvku na zaèátek seznamu.
**/

void htInsert ( tHTable* ptrht, tKey key, tData data ) {

    if ((*ptrht) == NULL)
        return;
    int index = hashCode(key);
    tHTItem *tmp = htSearch(ptrht, key);
    if(tmp != NULL) //aktualizace
    {
        tmp->key = key;
        tmp->data = data;
    }
    else //vlozeni
    {
        if ((tmp = malloc (sizeof(struct tHTItem))) == NULL) //alokuju misto na heapu
        {
            return;
        }
        tmp->key = key; //vlozim do tmp key klic co mi byl zadan
        tmp->data = data; //a data
        tmp->ptrnext = (*ptrht)[index]; //prehodim ukazatel
        (*ptrht)[index] = tmp; //nahraju do pole prvek
    }
}

/*
** TRP s explicitnì zøetìzenými synonymy.
** Tato funkce zji¹»uje hodnotu datové èásti polo¾ky zadané klíèem.
** Pokud je polo¾ka nalezena, vrací funkce ukazatel na polo¾ku
** Pokud polo¾ka nalezena nebyla, vrací se funkèní hodnota NULL
**
** Vyu¾ijte døíve vytvoøenou funkci HTSearch.
*/

tData* htRead ( tHTable* ptrht, tKey key ) {
    if ((*ptrht) == NULL)
        return NULL;
    tHTItem *tmp = htSearch(ptrht, key);
    if(tmp != NULL)
        return &(tmp->data);
    else
        return NULL;

}

/*
** TRP s explicitnì zøetìzenými synonymy.
** Tato procedura vyjme polo¾ku s klíèem key z tabulky
** ptrht.  Uvolnìnou polo¾ku korektnì zru¹te.  Pokud polo¾ka s uvedeným
** klíèem neexistuje, dìlejte, jako kdyby se nic nestalo (tj. nedìlejte
** nic).
**
** V tomto pøípadì NEVYU®ÍVEJTE døíve vytvoøenou funkci HTSearch.
*/

void htDelete ( tHTable* ptrht, tKey key ) {
    if ((*ptrht) == NULL) // kdyz tabulka neexistuje skonci
        return;
    int index = hashCode(key); //ziskej index pole
    tHTItem *tmp = (*ptrht)[index]; // ulozim si prvek do tmp
    tHTItem *tmp2 = NULL; //tmp2

    while (tmp != NULL) //pokud neni NULL (tzn existuje)
    {
        if(tmp->key == key) //a pokud klice se shoduji
        {
            if(tmp == ((*ptrht)[index])) //pokud je prvni
            {
                (*ptrht)[index] = tmp->ptrnext; //tak prvek pole ukazuje na nasledovnika
            }
            else
            {
                tmp2->ptrnext = tmp->ptrnext; //tmp2 ukazuje nyni na nasledovnika tmp
            }
            free(tmp); //uvolni tmp
            return;
        }
        tmp2 = tmp;
        tmp = tmp ->ptrnext;
    }
    return;

}

/* TRP s explicitnì zøetìzenými synonymy.
** Tato procedura zru¹í v¹echny polo¾ky tabulky, korektnì uvolní prostor,
** který tyto polo¾ky zabíraly, a uvede tabulku do poèáteèního stavu.
*/

void htClearAll ( tHTable* ptrht ) {
    if ((*ptrht) == NULL)
        return;
    tHTItem *tmp = NULL;
    tHTItem *tmp2 = NULL;
    int index;
    for (index = 0; index < HTSIZE; index++)
    {
        tmp = (*ptrht)[index];
        (*ptrht)[index] = NULL;

        while (tmp != NULL)
        {
            tmp2 = tmp;
            tmp = tmp->ptrnext;
            if(strcmp("var", tmp2->data.value) == 0)
            {
                free(tmp2->data.localTable);
                free(tmp2->key);
                free(tmp2->data.type);
                free(tmp2);
            }
            else
            {
                free(tmp2->key);
                htClearAll(tmp2->data.localTable);
                free(tmp2->data.localTable);
                free(tmp2->data.type);
                free(tmp2);

            }
        }
    }
}


//------------------------------------------------------------------------------


void ComputeJumps(char *P, int CharJump[256]){
    unsigned char ch;
    int k;

    for(ch = 0 ; ch < 255 ; ch++){
        CharJump[ch] = strlen(P);
    }
    for(k = 0 ; k < strlen(P) ; k++){
        CharJump[(int)P[k]] = strlen(P) - k - 1;
    }
}

int BMAFind(char *P,char *T, int CharJump[256])
{
    int i;
    int j;
    int P_size = strlen(P);
    int T_sizej = strlen(T) - 1;

    if(T_sizej == -1) return 0;

    j = T_sizej;
    i = T_sizej;
    while(i < P_size)
    {
        while(P[i] == T[j]) {
            if(j == 0) {
                return i + 1;
            }
            j--;
            i--;
        }
        j = T_sizej;
        i = i + CharJump[(int)P[i]];
    }
    return 0;
}


void BoyerMoorAlogythm(char *str1, char *str2, int *ret) {
    int CharJump[256];
    ComputeJumps(str2, CharJump);
    *ret = BMAFind(str1, str2, CharJump);
}




void parririon(char *A, int left, int right, int *i, int *j) {
    int PM;
    *i = left;
    *j = right;
    PM = A[(*i+*j) / 2];
    do {
        while(A[*i] < PM) (*i)++;
        while(A[*j] > PM) (*j)--;
        if(*i <= *j) {
            if(*i != *j){
                A[*i] = A[*j] - A[*i];
                A[*j] = A[*j] - A[*i];
                A[*i] = A[*j] + A[*i];
            }
            (*i)++;
            (*j)--;
        }
    } while(*i <= *j);
}

void QuickSort(char *A, int left, int right){
    int i;
    int j;
    parririon(A, left, right, &i, &j);
    if(left < j)  QuickSort(A, left, j);
    if(i < right) QuickSort(A, i, right);
}







